# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

PYTHON_COMPAT=( python{3_4,3_5,3_6} )

inherit distutils-r1 user

DESCRIPTION="Mailman -- the GNU mailing list manager"
HOMEPAGE="http://www.list.org"
SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"

LICENSE="GPL-3+"
SLOT="0"
#KEYWORDS="~amd64 ~ppc ~x86"
KEYWORDS="~amd64 ~x86"  # nothing til this is finished
IUSE=""

DEPEND="dev-python/setuptools[${PYTHON_USEDEP}]"
RDEPEND="
	~dev-python/aiosmtpd-1.0[${PYTHON_USEDEP}]
	dev-python/alembic[${PYTHON_USEDEP}]
	dev-python/atpublic[${PYTHON_USEDEP}]
	>=dev-python/dnspython-1.14.0[${PYTHON_USEDEP}]
	>=dev-python/falcon-1.0.0[${PYTHON_USEDEP}]
	dev-python/flufl-bounce[${PYTHON_USEDEP}]
	>=dev-python/flufl-i18n-2.0.1[${PYTHON_USEDEP}]
	>=dev-python/flufl-lock-3.1[${PYTHON_USEDEP}]
	dev-python/lazr-config[${PYTHON_USEDEP}]
	dev-python/passlib[${PYTHON_USEDEP}]
	dev-python/requests[${PYTHON_USEDEP}]
	dev-python/sqlalchemy[${PYTHON_USEDEP}]
	dev-python/zope-component[${PYTHON_USEDEP}]
	dev-python/zope-configuration[${PYTHON_USEDEP}]
	dev-python/zope-event[${PYTHON_USEDEP}]
	dev-python/zope-interface[${PYTHON_USEDEP}]
"

pkg_postinst() {
	VAR_PREFIX=${MAILMAN_VAR_PREFIX:-"/var/lib/mailman3"}
	MAILUSR=${MAILMAN_MAILUSR:-mailman}
	MAILUID=${MAILMAN_MAILUID:-280}
	MAILGRP=${MAILMAN_MAILGRP:-mailman}
	MAILGID=${MAILMAN_MAILGID:-280}

	enewgroup ${MAILGRP} ${MAILGID}
	enewuser  ${MAILUSR} ${MAILUID} /bin/bash ${VAR_PREFIX} mailman,cron

	keepdir ${VAR_PREFIX}/logs
	keepdir ${VAR_PREFIX}/locks
	keepdir ${VAR_PREFIX}/spam
	keepdir ${VAR_PREFIX}/archives/public
	keepdir ${VAR_PREFIX}/archives/private
	keepdir ${VAR_PREFIX}/lists
	keepdir ${VAR_PREFIX}/qfiles
	##chown -R ${MAILUSR}:${MAILGRP} "${D}/${VAR_PREFIX}" "${D}"/etc/mailman/* || die

	# default to /var/lib/mailman3 for VAR_PREFIX
	}
