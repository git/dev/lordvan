# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/net-misc/rdesktop/rdesktop-1.6.0-r4.ebuild,v 1.1 2010/03/09 10:21:43 voyageur Exp $

EAPI=2

inherit eutils

MY_PV=${PV/_/-}

DESCRIPTION="RD Connection Manager is a free open source Remote Desktop (RD) management solution"
HOMEPAGE="http://rdpdesk.com/"
SRC_URI="mirror://sourceforge/${PN}/${PN}-${MY_PV}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

RDEPEND="x11-libs/wxGTK:2.8
	 dev-libs/openssl"

DEPEND="${RDEPEND}"

src_configure() {
	econf --with-wx-config=/usr/bin/wx-config-2.8 \
	      || die "configuration failed"
}

src_install() {
	#einstall || die "Install failed"
	emake DESTDIR="${D}" install || die "emake install failed"
}