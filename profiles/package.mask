#####################################################################
# $Header: $
# When you add an entry to this file, add your name, the date, and an
# explanation of why something is getting masked
#
# NOTE: Please add your entry at the top!
#

##
##  This is an example
##
# <bangert@gentoo.org> (28 Jun 2002)
# psypete says these are broken and i am using the
# opporturnity to test the masking style :)
# <bangert@gentoo.org> (28 Jun 2002)
# psypete says these are not really broken - its just
# the v4l stuff that does not work
#=media-video/mplayer-0.90_pre5
#=media-video/mplayer-0.90_pre5-r1
##
##   End example
##

# Thomas Raschbacher <lordvan@gentoo.org> (23 Apr 2011)
# testing this first
#=media-tv/me-tv-2.0.1

# Thomas Raschbacher <lordvan@gentoo.org> (12. Aug 2013)
# Not working for me so masking. There is an ebuild in
# the zugina overlay, but I haven't had a chance to test
# that one yet. (If it works for me I will remove openshot
# from my overlay)
>=media-video/openshot-1.2.2
