# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="VeriCAD"
HOMEPAGE="http://www.vericad.com/"
SRC_URI="x86? ( varicad2008-en_${PV}_i386.deb )
	 amd64? ( varicad2008-en_${PV}_amd64.deb )"

LICENSE=""
SLOT="0"

# NOTE: varicad binaries are supplied just pre-stripped.
RESTRICT="fetch strip"

KEYWORDS="~amd64 ~x86"

RDEPEND="!sci-misc/varicad-de
	x11-libs/libICE
	x11-libs/libSM
	x11-libs/libX11
	x11-libs/libXcursor
	x11-libs/libXext
	x11-libs/libXmu
	x11-libs/libXrender
	x11-libs/libXt
	virtual/opengl"

S="${WORKDIR}"

pkg_nofetch() {
	echo
	eerror "Please go to: http://www.varicad.com/"
	eerror
	eerror "and download the VariCAD for Linux 2008 ${PV}"
	eerror " Debian package for your platform"
	eerror "After downloading it, put the .deb into:"
	eerror "  ${DISTDIR}"
	echo
}

src_unpack() {
	unpack ${A}
	unpack ./data.tar.gz
	cd "${S}"

	# removing not necessary content
	rm control.tar.gz data.tar.gz debian-binary \
		opt/VariCAD/desktop/*.sh \
		usr/share/menu/varicad2008-en \
		opt/VariCAD/desktop/varicad.mdkmenu \
		opt/VariCAD/desktop/varicad.wmconfig

	# NOTE: we need to strip some (useless) quotes
	sed -i \
		-e "s:\"::g" opt/VariCAD/desktop/varicad.desktop \
		|| die "seding varicad.desktop failed"
}

src_install() {
	# creating the desktop menu
	domenu opt/VariCAD/desktop/varicad.desktop || die "domenu failed."
	domenu opt/VariCAD/desktop/x-varicad.desktop || die "domenu mime-type failed."
	doicon opt/VariCAD/desktop/{varicad_*.png,varicad.xpm} || die "doicon failed."
	rm opt/VariCAD/desktop/*.desktop \
		opt/VariCAD/desktop/varicad_*.png
	rm -r opt/VariCAD/desktop

	# installing the docs
	dodoc usr/share/doc/varicad2008-en/{README-en.txt,README.Debian,copyright,changelog.gz}
	rm usr/share/doc/varicad2008-en/*

	# installing VariCAD
	cp -pPR * "${D}"/ || die "installing data failed"
}
