# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils autotools flag-o-matic multilib

DESCRIPTION="Powerful Constructive Solid Geometry modeling system."
HOMEPAGE="http://brlcad.org/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.bz2"

LICENSE="LGPL-2 BSD BDL"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="debug opengl"

DEPEND="media-libs/libpng
	sys-libs/zlib
	dev-tcltk/itcl
	dev-tcltk/itk
	dev-tcltk/blt
	dev-tcltk/iwidgets
	media-libs/urt"

RDEPEND="${DEPEND}"

brlcadprefix="/usr/brlcad"

src_unpack() {
	unpack ${A}
	cd "${S}"
	#epatch "${FILESDIR}"/brlcad-tcltk-man_install.patch
	# eautoreconf doesn't seem to work, run autogen.sh
	epatch ${FILESDIR}/usr-local.patch
	#epatch "${FILESDIR}/brlcad-${PV}-tkBind.patch"
	filter-ldflags "-Wl,--as-needed"
	#sh autogen.sh
}

src_compile() {
	local myconf
	filter-ldflags "-Wl,--as-needed"
	# --disable-step-build to disable fedex build
	myconf="${myconf} \
		--enable-documentation \
		--enable-iwidgets-build \
		--enable-libblt-build \
		--enable-libtkimg-build \
		--enable-models \
		--disable-step-build \
		--enable-opennurbs-build"

	use debug &&
		myconf="${myconf} --enable-debug" ||
		myconf="${myconf} --disable-debug --enable-optimized \
		--disable-runtime-debug" 

	use opengl && myconf="${myconf} --with-ogl"

	./configure $myconf || die "configure failed"
	emake || die "emake failed"
}


src_install() {
einfo install
	
	emake DESTDIR="${D}" install || die "emake install failed"
	#fix broken symlink
	#cd ${D}/${brlcadprefix}/$(get_libdir)
	#rm librtserver.so
	#ln -s librt.so.19.0.1 librtserver.so
einfo "setting up /etc/env.d/99brlcad"
	dodir /etc/env.d || die
	echo "PATH=${brlcadprefix}/bin" > "${D}"/etc/env.d/99brlcad || die
	#echo "LDPATH=${brlcadprefix}/$(get_libdir)" >> "${D}"/etc/env.d/99brlcad || die
	#echo "MANPATH=${brlcadprefix}/man" >> "${D}"/etc/env.d/99brlcad || die
	#cd ${D}${brlcadprefix}
	#echo "ITK_LIBRARY=${brlcadprefix}/$(find $(get_libdir) -maxdepth 1 -type d -name 'itk*' -print)" >> "${D}"/etc/env.d/99brlcad || die
	#echo "ITCL_LIBRARY=${brlcadprefix}/$(find $(get_libdir) -maxdepth 1 -type d -name 'itcl*' -print)" >> "${D}"/etc/env.d/99brlcad || die
}

pkg_postinst() {
	einfo "The standard starting point for BRL-CAD is the mged"
	einfo "command.  Examples are available in ${brlcadprefix}/db."
	einfo "To run an example, try:"
	einfo "${brlcadprefix}/bin/mged ${brlcadprefix}/share/brlcad/${PV}/db/havoc.g"
	einfo "In the mged terminal window, type 'draw havoc' to see"
	einfo "the wireframe in the visualization window."
}
