# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="VeriCAD viewer."
HOMEPAGE="http://www.vericad.com/"
SRC_URI="x86? ( varicad2008-view-en_${PV}_i386.deb )
	 amd64? ( varicad2008-view-en_${PV}_amd64.deb )"

LICENSE=""
SLOT="0"

# NOTE: varicad binaries are supplied just pre-stripped.
RESTRICT="fetch strip"

KEYWORDS="~amd64 ~x86"

RDEPEND="!sci-misc/varicad-viewer-de
	x11-libs/libICE
	x11-libs/libSM
	x11-libs/libX11
	x11-libs/libXcursor
	x11-libs/libXext
	x11-libs/libXmu
	x11-libs/libXrender
	x11-libs/libXt
	virtual/opengl"

S="${WORKDIR}"

pkg_nofetch() {
	echo
	eerror "Please go to: http://www.varicad.com/"
	eerror
	eerror "and download the VariCAD Viewer for Linux 2008 ${PV}"
	eerror " Debian package for your platform"
	eerror "After downloading it, put the .deb into:"
	eerror "  ${DISTDIR}"
	echo
}

src_unpack() {
	unpack ${A}
	unpack ./data.tar.gz
	cd "${S}"

	# removing not necessary content
	rm control.tar.gz data.tar.gz debian-binary \
		opt/VariCAD-View/desktop/*.sh \
		usr/share/menu/varicad2008-view-en \
		opt/VariCAD-View/desktop/varicadview.mdkmenu \
		opt/VariCAD-View/desktop/varicadview.wmconfig

	# NOTE: we need to strip some (useless) quotes
	sed -i \
		-e "s:\"::g" opt/VariCAD-View/desktop/varicadview.desktop \
		|| die "seding varicadview.desktop failed"
}

src_install() {
	# creating the desktop menu
	domenu opt/VariCAD-View/desktop/varicadview.desktop || die "domenu failed."
	domenu opt/VariCAD-View/desktop/x-varicadview.desktop || die "domenu mime-type failed."
	doicon opt/VariCAD-View/desktop/{varicadview_*.png,varicadview.xpm} || die "doicon failed."
	rm opt/VariCAD-View/desktop/*.desktop \
		opt/VariCAD-View/desktop/varicadview_*.png
	rm -r opt/VariCAD-View/desktop

	# installing the docs
	dodoc usr/share/doc/varicad2008-view-en/{README-en.txt,README.Debian,copyright,changelog.gz}
	rm usr/share/doc/varicad2008-view-en/*

	# installing VariCAD
	cp -pPR * "${D}"/ || die "installing data failed"
}